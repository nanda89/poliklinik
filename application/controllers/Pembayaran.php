<?php
class Pembayaran extends CI_Controller{

            function __construct(){
                parent::__construct();

                  $this->load->model('model_datamaster');
                  $this->load->model('model_pembayaran');
                  $this->load->model('model_dokter');
                  $this->load->model('model_apoteker');
                  $this->load->model('model_pendaftaran');
                  $this->load->library('pagination');

            }

            function index($id = NULL){

              $total_rows=$this->model_datamaster->read_pendaftar(NULL)->num_rows();

              $config['base_url']=base_url()."pembayaran";
              $config['total_rows']=$total_rows;
              $config['per_page']=15;
              $config['first_page']='Awal';
              $config['last_page']='Akhir';
              $config['next_page']='>>';
              $config['prev_page']='<<';

              $config['full_tag_open'] = '<ul class="pagination">';
              $config['full_tag_close'] = '</ul>';
              $config['first_link'] = '&laquo; First';
              $config['first_tag_open'] = '<li class="prev page">';
              $config['first_tag_close'] = '</li>';

              $config['last_link'] = 'Last &raquo;';
              $config['last_tag_open'] = '<li class="next page">';
              $config['last_tag_close'] = '</li>';

              $config['next_link'] = 'Next &rarr;';
              $config['next_tag_open'] = '<li class="next page">';
              $config['next_tag_close'] = '</li>';

              $config['prev_link'] = '&larr; Prev';
              $config['prev_tag_open'] = '<li class="prev page">';
              $config['prev_tag_close'] = '</li>';

              $config['cur_tag_open'] = '<li class="current"><a href="">';
              $config['cur_tag_close'] = '</a></li>';

              $config['num_tag_open'] = '<li class="page">';
              $config['num_tag_close'] = '</li>';
              $this->pagination->initialize($config);

              

              $data['halaman']=$this->pagination->create_links();
              $data['query']=$this->model_datamaster->read_pendaftar_all($config['per_page'],$id)->result();

              $this->load->view('page/data_pembayaran/vw_pembayaran',$data);

            }

            function bayar($kode,$kode_psn){
                // var_dump($kode);
                $fullData = $this->model_dokter->get_all_data_by_no_pendaftaran($kode)->row();
                $obat = $fullData->diagnosa;
                $kode_dkt = $fullData->kode_dkt;
                $data['dokter'] = $this->model_datamaster->read_dokter($kode_dkt)->row();
                // var_dump($data['dokter']->tarif);
                $total = $fullData->biaya + $data['dokter']->tarif;

                $data['subTotal'] = $total;
                $data['obat'] = array();

                $filter = array(
                    'no_pdf'      => $kode,
                );

                $dataResep = $this->model_dokter->get_resep($filter)->row();
                $nomor_resep = $dataResep->nomor_resep;
                // var_dump($nomor_resep);
                $where = array(
                    'nomor_resep'      => $nomor_resep,
                );
                $totRowDetail = $this->model_datamaster->get_detail_by_resep($where)->num_rows();
                
                if($totRowDetail > 0){
                    
                    $data['obat'] = $this->model_datamaster->get_detail_by_resep($where)->result();
                    
                    foreach($data['obat'] as $dt){
                        $total = $total + $dt->sub_total;
                    }
                    $data['subTotal'] = $total;
                }
                    
              $data['admin'] = $fullData->biaya;
              
              $data['query'] = $this->model_pembayaran->read_pendaftar(decr($kode))->result();
              $data['query2'] = $this->model_dokter->get_resepinfov2(decr($kode_psn))->result();
              $result[] = array();
                foreach($data['query2'] as $k){
                    $result[] = $k->sub_total;
                    
                }
              $data['totalharga'] = array_sum($result);
              $data['resep'] = $nomor_resep;
              $data['dokter'] = $this->model_datamaster->read_dokter($kode_dkt)->row();
              $this->load->view('page/data_pembayaran/vw_bayar',$data);
            }

            function cetak($kode,$kode_psn){

                $fullData = $this->model_dokter->get_all_data_by_no_pendaftaran($kode)->row();
                $obat = $fullData->diagnosa;
                $kode_dkt = $fullData->kode_dkt;
                $data['dokter'] = $this->model_datamaster->read_dokter($kode_dkt)->row();
                // var_dump($data['dokter']->tarif);
                $total = $fullData->biaya + $data['dokter']->tarif;

                $data['subTotal'] = $total;
                $data['obat'] = array();

                $filter = array(
                    'no_pdf'      => $kode,
                );

                $dataResep = $this->model_dokter->get_resep($filter)->row();
                $nomor_resep = $dataResep->nomor_resep;

                $where = array(
                    'nomor_resep'      => $nomor_resep,
                );
                $totRowDetail = $this->model_datamaster->get_detail_by_resep($where)->num_rows();
                
                if($totRowDetail > 0){
                    
                    $data['obat'] = $this->model_datamaster->get_detail_by_resep($where)->result();
                    
                    foreach($data['obat'] as $dt){
                        $total = $total + $dt->sub_total;
                    }
                    $data['subTotal'] = $total;
                }
                    
              $data['admin'] = $fullData->biaya;
              
              $data['query'] = $this->model_pembayaran->read_pendaftar(decr($kode))->result();
              $data['query2'] = $this->model_dokter->get_resepinfov2(decr($kode_psn))->result();
              $result[] = array();
                foreach($data['query2'] as $k){
                    $result[] = $k->sub_total;
                    
                }
              $data['totalharga'] = array_sum($result);
              $data['resep'] = $fullData->nomor_resep;
              $data['dokter'] = $this->model_datamaster->read_dokter($kode_dkt)->row();
              $data['transaksi'] = $dataResep;
              $this->load->view('page/data_pembayaran/print',$data);
            }
    
            function owa($kode_psn,$total){
                $data = array(
                    'nomor_byr'      => random_chara("BYR_"),
                    'kode_psn'       => ambil('kode_psn009') ,
                    'tanggal_byr'    => date('d-m-Y'),
                    'jumlah_bayar'   => ambil('total009'),
                );
                if ($this->model_pembayaran->bayar_langsung($data)){
                    echo "Berhasil
                    <br> Print PDF
                    ";
                }else{
                    echo "Tidak berhasil";
                }
            }
    
            function struk_new($kode_pendaftaran,$kode_psn){
                
                $bayar  = ambil('jumlah_bayar');
                $bayar_ = str_replace(",","",$bayar);
                $bayar_ = str_replace(".00","",$bayar_);

                $tot_bayar  = ambil('total_bayar');
                $kembali    = ambil('jumlah_kembalian');
                $resep      = ambil('resep');

                $data = array(
                    'bayar'         => $bayar_,
                    'kembali'       => $kembali,
                    'total_harga'   => $tot_bayar,
                );
                // var_dump($resep);
                $this->model_apoteker->update_data('nomor_resep',$resep,$data,'resep');

                $data2 = array(
                    'nomor_byr'         => random_chara("BYR_"),
                    'kode_psn'          => $kode_psn,
                    'tanggal_byr'       => date('d-m-Y'),
                    'jumlah_bayar'      => $bayar_,
                );
                // var_dump($data2);
                $this->model_pembayaran->bayar_langsung($data2);
                $resStruk['resep']      =$resep;
                $resStruk['sub_total']  =$tot_bayar;
                $resStruk['bayar']      =$bayar;
                $resStruk['kembali']    =$kembali;
                redirect('/pembayaran/cetak/'.$kode_pendaftaran."/".$kode_psn, 'refresh');
            }
                function struk($kode,$kode2){
                    $this->load->model('model_apoteker');
                    $data = array(
                        'bayar'      => ambil('jumlah_bayar'),
                        'kembali'   => ambil('kembalian_s'),
                    );
                    $this->model_apoteker->update_data('nomor_resep',$kode2,$data,'resep');
                    
                    $data2 = array(
                        'nomor_byr'         => random_chara("BYR_"),
                        'kode_psn'          => $kode,
                        'tanggal_byr'       => date('d-m-Y'),
                        'jumlah_bayar'      => ambil('jumlah_bayar'),
                    );
                    $from['jumlah_bayar'] = ambil('jumlah_bayar');
                    $from['kembalian']    = ambil('kembalian_s');
                    
                    if ($this->model_pembayaran->bayar_langsung($data2)){
                        $from['query2'] = $this->model_dokter->get_resepinfov2(decr($kode))->result();
                        $this->load->view('page/data_pembayaran/vw_struk',$from);
                    }
                }
    
            
    
            
}
