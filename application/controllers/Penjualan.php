<?php
class Penjualan extends CI_Controller{

    function __construct(){
        parent::__construct();

          $this->load->model('model_datamaster');
          $this->load->model('model_pembayaran');
          $this->load->model('model_dokter');
          $this->load->library('pagination');

    }

    function index($id = NULL){
        $total_rows=$this->model_datamaster->read_pendaftar(NULL)->num_rows();

              $config['base_url']=base_url()."pembayaran";
              $config['total_rows']=$total_rows;
              $config['per_page']=15;
              $config['first_page']='Awal';
              $config['last_page']='Akhir';
              $config['next_page']='>>';
              $config['prev_page']='<<';

              $config['full_tag_open'] = '<ul class="pagination">';
              $config['full_tag_close'] = '</ul>';
              $config['first_link'] = '&laquo; First';
              $config['first_tag_open'] = '<li class="prev page">';
              $config['first_tag_close'] = '</li>';

              $config['last_link'] = 'Last &raquo;';
              $config['last_tag_open'] = '<li class="next page">';
              $config['last_tag_close'] = '</li>';

              $config['next_link'] = 'Next &rarr;';
              $config['next_tag_open'] = '<li class="next page">';
              $config['next_tag_close'] = '</li>';

              $config['prev_link'] = '&larr; Prev';
              $config['prev_tag_open'] = '<li class="prev page">';
              $config['prev_tag_close'] = '</li>';

              $config['cur_tag_open'] = '<li class="current"><a href="">';
              $config['cur_tag_close'] = '</a></li>';

              $config['num_tag_open'] = '<li class="page">';
              $config['num_tag_close'] = '</li>';
              $this->pagination->initialize($config);

              $data['obat'] = $this->model_datamaster->getAllObat()->result();
              

              $data['halaman']=$this->pagination->create_links();
              $data['query']=$this->model_datamaster->read_pendaftar_all($config['per_page'],$id)->result();

        $this->load->view('page/data_penjualan/vm_penjualan',$data);
    }
    function find_obat(){
        $var= ambil("code");

        $get=$this->model_datamaster->get_obat($var)->row();

        $array_data= array(
            "kode"      => $get->kode_obat,
            "nama"      => $get->nama_obat,
            "harga"     => $get->harga_obat,
            "jenis"     => $get->jenis_obat,
            "jumlah"    => $get->jumlah_obat,
          );

        // foreach ($get as $k):
        //   $array_data[]= array(
        //     "kode"      => $k->kode_obat,
        //     "nama"      => $k->nama_obat,
        //     "harga"     => $k->harga_obat,
        //     "jumlah"    => $k->jumlah_obat,
        //   );
        // endforeach;
        echo json_encode($array_data);
     }
     function do_bayar(){
         $header = $this->input->post('penjualan');
         $detail = $this->input->post('detail');
         
        

         $h = json_encode($header);
         $headData = json_decode($h);

         $d = json_encode($detail);
         $detailData = json_decode($d);

         $user = $this->session->userdata('kodenameuser');
         $inv = date('ymd')."/".$user."-".rand(100,999);
         $data = array(
            'total'         => $headData->total,
            'cash'          => $headData->cash ,
            'created_date'  => date('d-m-Y'),
            'kembali'       => $headData->kembali,
            'user'          => $user,
            'invoice'       => $inv,
        );

        $id = $this->model_datamaster->insert_penjualan($data);

        foreach($detailData as $key => $val){

            $qty = $val->qty;
            $sub = $val->sub_total;
            $dataDetail[] = array(
                'code_obat'     => $val->code,
                'qty'           => (int)$qty,
                'sub_total'     => (int)$sub,
                'penjualan_id'  => $id,
                );
            
         }
         $res = $this->model_datamaster->insert_detail_penjualan($dataDetail);

         $obj = new stdClass();

         if($res){

            foreach($detailData as $key => $val){
                $qty = $val->qty;
                
                $obat = $this->model_datamaster->get_obat($val->code)->row();
                $tot_obat = $obat->jumlah_obat - (int)$qty;

                $dataSet = array(
                    'jumlah_obat' => $tot_obat,
                  );

                  $this->model_datamaster->update_obat($dataSet , $val->code);
            }



            $obj->code      = 200;
            $obj->message   = 'data berhasil disimpan';
            $obj->id        = $id;
         }else{
            $obj->code      = 400;
            $obj->message   = 'data gagal disimpan';
         }
         
         echo json_encode($obj);
     }

     function cetak($id){
        $filter = array(
            'id' => $id
          );
        $data['penjualan'] = $this->model_datamaster->get_list($filter , 'penjualan')->row();

        $filter = array(
            'id' => $data['penjualan']->user
          );
        $data['user'] = $this->model_datamaster->get_list($filter , 'akun')->row();
        
        $filter = array(
          'name' => 'data',
        );
        $conf = $this->model_datamaster->get_config($filter , 'config')->row();
        $data['conf'] = json_decode($conf->value);

        
        $filterDetail = array(
            'penjualan_detail.penjualan_id' => $id
          );
        $data['penjualan_detail'] = $this->model_datamaster->get_list_join($filterDetail , 'penjualan_detail','obat','penjualan_detail.code_obat = obat.kode_obat')->result();
        
        $this->load->view('page/data_penjualan/print',$data);
     }
}