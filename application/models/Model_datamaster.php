<?php
class Model_datamaster extends CI_Model{

  function read_dataobat(){
    return $this->db->get('obat');
  }
  function get_obat_by_name($name){
    $this->db->where('nama_obat',$name);
    return $this->db->get('obat');
  }

  function getAllObat(){
    return $this->db->query('select * from obat');
  }

  function get_obat_by_where_in($column , $in){
    $this->db->where_in($column ,$in );
    return $this->db->get('obat');
  }

  function get_all($num,$offset,$table){
    return $this->db->get($table,$num,$offset);
  }
  function cari_dataobat($like){
    $this->db->like('nama_obat',$like);
    return $this->db->get('obat');
  }
  function cari_dataobat1($like){
    return $this->db->query('select * from obat where nama_obat like "%'.$like.'%"');
  }
  function tambah_obat($data){
    $this->db->set($data);
    return $this->db->insert('obat');
  }
  function hapus_obat($where){
    $this->db->where($where);
    return $this->db->delete('obat');
  }
  function get_obat($where){
    $this->db->where('kode_obat',$where);
    return $this->db->get('obat');
  }
  function update_obat($data,$where){
    $this->db->set($data);
    $this->db->where('kode_obat',decr($where));
    return $this->db->update('obat');
  }
  // ----- pasien
  function read_pasien($where){
    if (NULL ==! $where){
      $this->db->where('kode_psn',$where);
    }
    return $this->db->get('pasien');
  }
  function insert_pasien($data){
    $this->db->set($data);
    return $this->db->insert('pasien');
  }
  function delete_pasien($where){
    $this->db->where('kode_psn',$where);
    return $this->db->delete('pasien');
  }
  function update_pasien($data,$where){
    $this->db->set($data);
    $this->db->where('kode_psn',decr($where));
    return $this->db->update('pasien');
  }
  function generateCode(){
    $year   = date('y');
    $query  = "select * from pasien where year(created_date) =".date('Y');
    $query  = $this->db->query($query);
    $tot_rows    = $query->num_rows();
    $tot_rows = $tot_rows + 1;

    if($tot_rows < 10){

      $code = $year."000".$tot_rows;

    }else if($tot_rows >= 10 && $tot_rows < 100){

      $code = $year."00".$tot_rows;

    }else if($tot_rows >= 100 && $tot_rows < 1000){

      $code = $year."0".$tot_rows;
    }else{
      $code = $year."".$tot_rows;
    }

    return $code;
  }
  // ------ Dokter
  function read_dokter($where){
    if (NULL ==! $where){
      $this->db->where('kode_dkt',$where);
    }
    return $this->db->get('dokter');
  }
  function insert_dokter($data){
    $this->db->set($data);
    return $this->db->insert('dokter');
  }
  function delete_dokter($where){
    $this->db->where('kode_dkt',$where);
    return $this->db->delete('dokter');
  }
  function update_dokter($data,$where){
    $this->db->set($data);
    $this->db->where('kode_dkt',decr($where));
    return $this->db->update('dokter');
  }
  // -------- poliklinik
  function read_poliklinik(){
    return $this->db->get('poliklinik');
  }
  function insert_poliklinik($data){
    $this->db->set($data);
    return $this->db->insert('poliklinik');
  }
  function del_poliklinik($where){
    $this->db->where('kode_plk',decr($where));
    return $this->db->delete('poliklinik');
  }
  //-----
  function read_pendaftar_all($num,$offset){
    $this->db->select('*');
    $this->db->from('pendaftaran');
    $this->db->join('pasien','pasien.kode_psn = pendaftaran.kode_psn');
    $this->db->join('dokter','dokter.kode_dkt = pendaftaran.kode_dkt');
    return $this->db->get('',$num,$offset);
  }
  function read_pendaftar(){
    return $this->db->get('pendaftaran');
  }
  function read_pendaftar_where($where){
    $this->db->select('*');
    $this->db->from('pendaftaran');
    $this->db->join('pasien','pasien.kode_psn = pendaftaran.kode_psn');
    $this->db->join('dokter','dokter.kode_dkt = pendaftaran.kode_dkt');
    $this->db->join('resep','resep.kode_psn = pendaftaran.kode_psn');
    $this->db->where('pendaftaran.nomor_pdf',$where);
    return $this->db->get();
  }
  function del_pendaftar($where){
    $this->db->where('nomor_pdf',decr($where));
    return $this->db->delete('pendaftaran');
  }

  // -------- Random
  function get_poliklinik(){
    return $this->db->get('poliklinik');
  }
  function like_data($like,$table){
    $this->db->like($like);
    return $this->db->get($table);
  }

  //---------- Pendaftaran
  function get_list_pendaftaran($where){
    if (NULL ==! $where){
      $this->db->where($where);
    }
    return $this->db->get('pendaftaran');
  }

  //----------- Penjualan
  function insert_penjualan($data){
    $this->db->insert('penjualan', $data);
    $insert_id = $this->db->insert_id();

   return  $insert_id;
  }

  function insert_detail_penjualan($data){
    return $this->db->insert_batch('penjualan_detail',$data);
  }

  function get_list($where , $table){
    if (NULL ==! $where){
      $this->db->where($where);
    }
    return $this->db->get($table);
  }

  function get_list_join($where , $table , $tableJoin , $filter){
    $this->db->select('*');
    $this->db->from($table);
    $this->db->join($tableJoin, $filter);
    if (NULL ==! $where){
      $this->db->where($where);
    }
    return $this->db->get();
  }
  /*
    query to config table
  */
  function get_config($where , $table){
    if (NULL ==! $where){
      $this->db->where($where);
    }
    return $this->db->get($table);
  }

  function get_detail_by_resep($where){
    if (NULL ==! $where){
      $this->db->where($where);
    }
    return $this->db->get('detail');
  }
}
