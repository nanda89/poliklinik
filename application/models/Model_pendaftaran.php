<?php
    class Model_pendaftaran extends CI_Model{

        function update_pendaftaran_by_kode($data,$no_pdf){
            $this->db->set($data);
            $this->db->where('nomor_pdf',$no_pdf);
            return $this->db->update('pendaftaran');
        }

        function get_data_by_no_pdf($no_pdf){
            $this->db->where('nomor_pdf',$no_pdf);
            return $this->db->get('pendaftaran');
          }

}