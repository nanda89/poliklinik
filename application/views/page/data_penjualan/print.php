<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../assets/AdminLTE-2.0.5/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../assets/AdminLTE-2.0.5/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../assets/AdminLTE-2.0.5/dist/css/skins/_all-skins.min.css">
</head>
<body>

  <!-- Content Wrapper. Contains page content -->
  
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-md-4 invoice-col">
          From
          <address>
            <strong><?php echo $conf->name ?></strong><br>
            <?php echo $conf->alamat ?><br>
            <?php echo $conf->phone ?><br>
            <?php echo $conf->email ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-md-4 invoice-col">
          To
          <address>
            <strong>Bapak/Ibu</strong><br>
            ...<br>
            ...<br>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-md-4 invoice-col">
          <b>Invoice #</b><?php echo $penjualan->invoice; ?><br>
          <br>
          <b>Tanggal Transaksi:</b> <?php echo $penjualan->created_date; ?><br>
          <b>Kassir:</b> <?php echo $user->nama; ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Nama Obat</th>
              <th>Harga Obat</th>
              <th>Qty</th>
              <th>Sub Total</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($penjualan_detail as $detail): ?>
            <tr>
              <td><?php echo $detail->nama_obat ?></td>
              <td><?php echo num_format($detail->harga_obat) ?></td>
              <td><?php echo $detail->qty ?></td>
              <td><?php echo num_format($detail->sub_total) ?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Total Pembayaran</p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Toal:</th>
                <td><span id="total"></span><?php echo num_format($penjualan->total) ?></td>
              </tr>
              <tr>
                <th>Cash:</th>
                <td><span id="cash"><?php echo num_format($penjualan->cash) ?></span></td>
              </tr>
              <tr>
                <th>Kembali:</th>
                <td><span id="kembali"></span><?php echo num_format($penjualan->kembali) ?></td>
              </tr>
            </table>
          </div>
        </div>
    </section>
  
<script type="text/javascript">

    window.print();
</script>

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jQuery/jquery-2.1.3.min.js') ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/bootstrap/js/bootstrap.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/fastclick/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/app.min.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/demo.js') ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/mask/wNumb.js') ?>"></script>
</body>
</html>
