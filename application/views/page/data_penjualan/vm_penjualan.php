<?php
$this->load->view('page/template/head');
?>

<!--tambahkan custom css disini-->
<!-- iCheck -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/flat/blue.css') ?>" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.css') ?>" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.css') ?>" rel="stylesheet" type="text/css" />
<!-- Date Picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>" rel="stylesheet" type="text/css" />
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" /> -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
<?php
$this->load->view('page/template/topbar');
$this->load->view('page/template/sidebar');
?>

<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>
        Penjualan
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">penjualan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
    <div class="col-md-6">
    <div class="box box-danger" >
          <div class="box-header with-border">
            <h3 class="box-title">Data Obat</h3>
          </div>
          <!-- /.box-header -->
          <form class="form-horizontal" >
            <div class="box-body">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Nama obat</label>
                <div class="col-sm-10">
                <select onchange="get_dataObat(this.value)" class="form-control selectpicker" id="select-obat" data-live-search="true">
                <?php foreach($obat as $k): ?>
                <option value = "<?php echo $k->kode_obat; ?>" data-tokens="<?php echo $k->nama_obat; ?>"><?php echo $k->nama_obat; ?></option>
                <?php endforeach; ?>
                </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Jenis obat</label>

                <div class="col-sm-10">
                  <input type="hidden" class="form-control" id = "code_obat"  name="code_obat" >
                  <input type="text" class="form-control" id = "jenis_obat" placeholder="Jenis obat" name="jeni_obat" required="true" disabled>
                </div>
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Harga obat</label>

                <div class="col-sm-10">
                  <input type="text" class="form-control" id = "harga_obat_view" placeholder="Harga" name="kategor" required="true" disabled>
                  <input type="hidden" class="form-control" id = "harga_obat" placeholder="Harga" name="kategor" required="true">
                </div>
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Stok</label>

                <div class="col-sm-10">
                  <input type="text" class="form-control" id="tot_obat"  name="tot_obat" disabled>
                  
                </div>
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Qty</label>

                <div class="col-sm-10">
                  
                  <input type="number" class="form-control" id="qty" placeholder="Quantity" name="qty" onblur=countSubTotal(this.value)>
                </div>
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Sub Total</label>

                <div class="col-sm-10">
                  <input type="text" class="form-control" id="sub_total_view" placeholder="Sub Total" name="sub_total" required="true" disabled>
                  <input type="hidden" class="form-control" id="sub_total" placeholder="Sub Total" name="sub_total" required="true">
                </div>
              </div>
              
            </div>

            <div class="box-footer">
              <button type="button" onclick ="pushTotable()" class="btn btn-primary">Tambah</button>
              <button type="reset" class="btn btn-default">Reset</button>

            </div>
          </form>
        </div>

    </div>
      <div class="col-md-6">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Pesanan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
</div><!-- /.row -->

              <table class="table table-bordered table-striped" id="table-penjualan">
                <thead>

                <tr>
                  <th>Kode</th>
                  <th>Nama Obat</th>
                  <th>Harga Obat</th>
                  <th>Qty</th>
                  <th>Sub Total</th>
                  <th>Action</th>
                </tr>

                </thead>
                <tbody>
                  

              </tbody>
              
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <div class="col-md-3">
          </div>
          <div class="col-md-3">
            <div class="box">
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <th style="width:30%"> Total</th>
                        <th> : </th>
                        <td><span id="final_sub_total"></span>
                            <span id="final_sub_total_view"></span></td>
                      </tr>
                      <tr>
                        <th style="width:30%"> Cash</th>
                        <th> : </th>
                        <td>
                        <!-- <input type="number" class="form-control" id="cash" onblur="hitung(this.value)"  disabled/> -->
                        <input type="text" class="form-control" id="cash" onblur="hitung(this.value)" data-inputmask='"alias": "currency"' data-mask  disabled/>
                        </td>
                      </tr>
                      <tr>
                        <th style="width:30%"> Kembali</th>
                        <th> : </th>
                        <td><span id="kembali"></span></td>
                      </tr>
                    </tbody>
                  </table>
                  <button type="button" id ="bayar" onclick="pembayaran()" class="btn btn-success pull-right" disabled>
                    <i class="fa fa-credit-card"> Bayar</i>
                  </button><br><br><br>
                </div>
                
            </div>
        </div>
        </div>
        
        <!-- /.col -->
      </div>
      <!-- /.row -->

</section><!-- /.content -->



<?php
$this->load->view('page/template/js');
?>

<!--tambahkan custom js disini-->
<!-- jQuery UI 1.11.2 -->
<script>

</script>
<script src="<?php echo base_url('assets/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>

$( document ).ready(function() {
  $('#cash').maskMoney();
});

    $.widget.bridge('uibutton', $.ui.button);
    var moneyFormat = wNumb({
                        mark: ',',
                        decimals: 0,
                        thousand: '.',
                        prefix: 'Rp. ',
                        suffix: ''
                    });

    $(function() {
      $('.selectpicker').selectpicker();
    });
    function get_dataObat(kode){
      
      $.ajax({
                  type : "POST",
                  dataType: "JSON",
                  url : "<?php echo site_url('penjualan/find_obat'); ?>",
                  data : {code : kode},
                  success:function(data){

                    console.log(data);
                    if(data != null){
                      clear();
                      $("#code_obat").val(data.kode);
                      $("#jenis_obat").val(data.jenis);
                      $("#harga_obat_view").val(moneyFormat.to(parseInt(data.harga)));
                      $("#harga_obat").val(data.harga);
                      $("#tot_obat").val(data.jumlah);
                    }
                    // $.each(data, function(i,n){
                    //   $("#view_dokter").append(
                    //     '<option value="'+n.kode_dkt+'">'+n.nama_dkt+'</option>'
                    //   );
                    // });
                  }
                })

    }

    function clear(){
      console.log("clear function")
      $("#code_obat").val('');
      $("#jenis_obat").val('');
      $("#harga_obat_view").val('');
      $("#harga_obat").val('');
      $("#qty").val('');
      $("#sub_total_view").val('');
      $("#sub_total").val('');
      $("#tot_obat").val('');
    }
    function countSubTotal(value){
      console.log(value);
      var tot_obat = $("#tot_obat").val();
      console.log("total obat :"+tot_obat);
      if(parseInt(value) < parseInt(tot_obat)){
        var harga = $("#harga_obat").val();
        console.log("harga : "+harga);
        var subTotal = harga * value;
        console.log("subTotal : "+subTotal);

        $("#sub_total_view").val(moneyFormat.to(subTotal));
        $("#sub_total").val(subTotal);
      }else{
        alert("Error : jumlah pesanan melebihi kapasitas barang");
        clear();
      }

      
    }
    function pushTotable(){
      var kode = $("#code_obat").val();
      var nama = $("#select-obat option:selected").text();
      var harga = $("#harga_obat_view").val();
      var qty = $("#qty").val();
      var sub_total =$("#sub_total_view").val();

      var rowCount = $('#table-penjualan tr').length;
      console.log(rowCount);
      console.log("harga : "+harga);
      if(harga == "" || qty == "" || sub_total==""){
        alert("Error : Pastikan anda sudah memasukan data dengan benar");
      }else{
        $('#table-penjualan tr:last').after('<tr id="row_'+rowCount+'"><td class="code">'+kode+'</td><td>'+nama+'</td><td class="harga">'+harga+'</td><td class="qty">'+qty+'</td><td class="sub">'+sub_total+'</td><td><button type="button" class="btn btn-danger" onclick="removeRow('+rowCount+')"><i class="fa fa-trash"></i></button></td></tr>');
        calculate();
        clear();
      }

      
    }
    function removeRow(row){
      $("#row_"+row).remove();
      calculate();
    }
    function calculate(){
      var total = 0;
      $('#table-penjualan tr').each(function() {
        var sub = $(this).find(".sub").html(); 
        if(typeof(sub) != "undefined" ){
          console.log("original value : "+sub);
          var sub_ = sub.trim().replace("Rp","").replaceAll(".","");

          console.log("sub total : "+sub_); 
          total += parseInt(sub_);
        }  
        
      });
      console.log("------->> "+total);

      var moneyFormat = wNumb({
                        mark: ',',
                        decimals: 0,
                        thousand: '.',
                        prefix: 'Rp. ',
                        suffix: ''
                    });

      $("#final_sub_total").text(moneyFormat.to(total));
      isEnable();
    }

    function isEnable(){
      var total = $("#final_sub_total").text();
      console.log("nilai total : "+total);
      if(total == "0" || total == ""){
        $("#cash").prop('disabled', true);
      }else{
        $("#cash").prop('disabled', false);
      }
    }
    function hitung(nilai){
      var total = $("#final_sub_total").text();
      var total_ = total.trim().replace("Rp","").replaceAll(".","");
      console.log("nilai total : "+total_);
      var nilai_ = nilai.trim().replaceAll(",","").replace(".00","");
      console.log("nilai berubah menjadi "+nilai_);
      if(total == "0" || total == ""){
        alert("Error : nilai total tidak dapat dihitung");
      }else{
        if(parseInt(nilai_) < parseInt(total_)){
          alert("Error : nilai cash tidak boleh lebih kecil dari total");
          $("#cash").val('');
          $("#kembali").text('');
          $("#bayar").attr("disabled" , true);
        }else{
          var kembali = parseInt(nilai_) - parseInt(total_);
          console.log("kembali : "+kembali);
          $("#kembali").text(moneyFormat.to(parseInt(kembali)));
          $("#bayar").attr("disabled" , false);
        }
      }
    }
    function pembayaran(){
      var total = $("#final_sub_total").text().trim().replace("Rp","").replaceAll(".","");
      var cash = $("#cash").val().trim().replaceAll(",","").replace(".00","");
      var kembali = $("#kembali").text().trim().replace("Rp","").replaceAll(".","");

      var myObj = new Object();
      myObj.total = parseInt(total);
      myObj.cash = parseInt(cash);
      myObj.kembali = parseInt(kembali);

      var detail = [];
      $('#table-penjualan tr').each(function() {
        var code = $(this).find(".code").html(); 
        var harga = $(this).find(".harga").html();
         
        var qty = $(this).find(".qty").html(); 
        var sub = $(this).find(".sub").html();
         
        if(typeof(harga) != "undefined" ){
          var harga_ = harga.trim().replace("Rp","").replaceAll(".","");
          var sub_ = sub.trim().replace("Rp","").replaceAll(".","");

          var d = new Object();
          d.code = code;
          d.harga = parseInt(harga_);
          d.qty = parseInt(qty);
          d.sub_total = parseInt(sub_);

          detail.push(d);
        }  
        
      });

      var obj = new Object();
      obj.penjualan = myObj;
      obj.detail = detail;

      var myString = JSON.stringify(obj);
      console.log(myString);
      var pathname = window.location.pathname.split('/');
      $.ajax({
        type : "POST",
        dataType: "JSON",
        url : "<?php echo site_url('penjualan/do_bayar'); ?>",
        data : obj,
        success:function(data){

            console.log("response : "+data.message);
            if(data != null){
              alert(data.message); 
              if(data.code == 200){
                var base_url = window.location.origin;
                window.location.replace(base_url+"/"+pathname[1]+"/penjualan/cetak/"+data.id);
              }else{
                location.reload();
              }
              
                    
            }
          }
        })
    }
</script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/js/raphael-min.js') ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.min.js') ?>" type="text/javascript"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/sparkline/jquery.sparkline.min.js') ?>" type="text/javascript"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>" type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/knob/jquery.knob.js') ?>" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker.js') ?>" type="text/javascript"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/bootstrap-datepicker.js') ?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/mask/jquery.maskMoney.js') ?>" type="text/javascript"></script>

<!-- Bootstrap WYSIHTML5 -->
<!-- <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>" type="text/javascript"></script> -->
<!-- iCheck -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/pages/dashboard.js') ?>" type="text/javascript"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/demo.js') ?>" type="text/javascript"></script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<?php
$this->load->view('page/template/foot');
?>
