<?php
$this->load->view('page/template/head');
?>

<!--tambahkan custom css disini-->
<!-- iCheck -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/flat/blue.css') ?>" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.css') ?>" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.css') ?>" rel="stylesheet" type="text/css" />
<!-- Date Picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>" rel="stylesheet" type="text/css" />
<script>
function hapus_confirm(){
  var msg;
  msg= "Apakah Anda Yakin Akan Menghapus Data ? " ;
  var agree=confirm(msg);
  if (agree)
  return true ;
  else
  return false ;
}
</script>
<?php
$this->load->view('page/template/topbar');
$this->load->view('page/template/sidebar');
?>

<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>
        Pembayaran
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">pembayaran</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

<div class="row">
<div class="col-xs-12">
<div class="box box-info">
            
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="nama_pasien_search" placeholder="Nama Pasien">
                  </div>
                  <div class="col-sm-3">
                    <button type="button" id="btn_search" class="btn btn-info pull-left">Cari</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
  </div>
</div>


    <div class="row">
      <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data pembayaran</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
</div><!-- /.row -->

              <table class="table table-bordered table-striped" id="table">
                <thead>

                <tr>
                  <th>No pendaftaran</th>
                  <th>Nama dokter</th>
                  <th>Nama pasien</th>
                  <th>Kode poliklinik</th>
                  <th>Biaya</th>
                  <th>Aksi</th>
                </tr>

                </thead>
                <tbody>
                  <?php foreach($query as $k): ?>
                <tr id="test">
                  <td><?php echo $k->nomor_pdf; ?></td>
                  <td><?php echo $k->nama_dkt; ?></td>
                  <td><?php echo $k->nama_psn; ?></td>
                  <td><?php echo $k->kode_plk; ?></td>
                  <td><?php echo num_format($k->biaya); ?></td>
                  <td><a class="btn btn-primary" href="<?php echo site_url('pembayaran/bayar/'. ency($k->nomor_pdf)."/".ency($k->kode_psn)); ?>" role="button"><i class="fa fa-edit"></i>Bayar</a> 
                      <a class="btn btn-danger" onClick="return hapus_confirm()" href="<?php echo site_url('datamaster/delete_pendaftar/'. ency($k->nomor_pdf)); ?>" role="button"><i class="fa fa-remove"></i>Hapus</a>  </td>
                </tr>
                <?php endforeach; ?>

              </tbody>
              <?php echo $halaman; ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

</section><!-- /.content -->



<?php
$this->load->view('page/template/js');
?>

<!--tambahkan custom js disini-->
<!-- jQuery UI 1.11.2 -->
<script>
var table = $("#table").DataTable({"paging": false,"searching": false});
$(document).ready(function(){
  $("#add_obat").click(function(){
    $("#tampil_addobat").toggle();
  })
});

</script>
<script>
$('#btn_search').click(function(){
  var nama_pasien = $('#nama_pasien_search').val();
  if(nama_pasien == ""){
    alert("nama pasien tidak boleh kosong !");
  }else{
    getDataPasien(nama_pasien);
  }
})
function getDataPasien(pasien){
  $.ajax({
    url: "<?=base_url()?>assets/file/getPembayaran.php?pasien="+pasien,
    method: 'get',
    dataType: 'json',
     success: function(result){
    
      console.log("total : "+result.length);
      if(result.length > 0){
        table.clear().draw();
        jQuery.each(result, function(index, item) {
          table.row.add([item.nomor_pdf,item.nama_dkt,item.nama_psn,item.kode_plk,item.biaya,
          '<a href="pembayaran/bayar/'+item.nomor_pdf+'/'+item.kode_psn+'"  class="btn btn-primary" role="button"><i class="fa fa-edit"></i>Bayar</a><a class="btn btn-danger" onClick="return hapus_confirm()" href="datamaster/delete_pendaftar/'+item.nomor_pdf+'" role="button"><i class="fa fa-remove"></i>Hapus</a>'
        ]).draw().node();
        });

      }else{
        alert("data yang anda cari tidak ditemukan.")
      }

      $('#nama_pasien_search').val('');
  }});
}
</script>
<script src="<?php echo base_url('assets/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/js/raphael-min.js') ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.min.js') ?>" type="text/javascript"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/sparkline/jquery.sparkline.min.js') ?>" type="text/javascript"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>" type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/knob/jquery.knob.js') ?>" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker.js') ?>" type="text/javascript"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/bootstrap-datepicker.js') ?>" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>" type="text/javascript"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/pages/dashboard.js') ?>" type="text/javascript"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/demo.js') ?>" type="text/javascript"></script>

<?php
$this->load->view('page/template/foot');
?>
