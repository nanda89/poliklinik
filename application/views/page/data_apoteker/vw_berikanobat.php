<?php
$this->load->view('page/template/head');
?>

<!--tambahkan custom css disini-->
<!-- iCheck -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/flat/blue.css') ?>" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.css') ?>" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.css') ?>" rel="stylesheet" type="text/css" />
<!-- Date Picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>" rel="stylesheet" type="text/css" />

<?php
$this->load->view('page/template/topbar');
$this->load->view('page/template/sidebar');
?>
<script>
    
        function select_obat(){
        var kategori = $("#kategori_obat").val();
        console.log(kategori);
        $.ajax({
            type : "POST"  ,
            dataType : "JSON",
            url : "<?php echo site_url('apoteker/pilih_obat'); ?>",
            data : {cat_obat : kategori},
            success:function(data){
                $.each(data, function(i,n){
                    $("#view_obat").append(
                        '<option value="'+n.kode_obat+'">'+n.nama_obat+' - '+n.jumlah_obat+'</option>'
                    );
                });
            }
        });
        $("#view_obat").empty();
        
    }
</script>

<!-- Content Header (Page header) -->
<section class="content-header">
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <!-- left column -->
         <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Pemberian obat</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php foreach($query as $k): ?>
                <div class="callout callout-danger">
                
              
                    <p>Diagnosa pasien : <?php echo $k->diagnosa; ?></p>
                  <p>Tanggal resep : <?php echo $k->tanggal_resep; ?> </p>
                  <p>Nama pasien : <?php echo $k->nama_psn; ?></p>
                  <p>Nama dokter : <?php echo $k->nama_dkt; ?></p>    
                  <p>Detail resep : <?php echo $k->detail_resep; ?></p>
              </div>      
                <hr>
                  <div class="row">
                    <div class="col-md-6">
                    <label>Pilih kategori obat</label>
                <div class="form-group">
                    
                    
                  <select class="form-control" id="kategori_obat" onclick="select_obat()" >
                      <?php foreach ($query_obat as $z): ?>
                    <option value="<?php echo $z->kategori; ?>"><?php echo $z->kategori; ?></option>
                      <?php endforeach; ?>
                  </select>
                    
                    <form role="form" action="<?php echo site_url('apoteker/proses_obat/'.ency($k->nomor_resep)."/".ency($k->nomor_pdf)); ?>" method="post">
                    <input type="hidden" id="no-pdf" value="<?php echo $k->no_pdf ?>"/>
                    <input type="hidden" id="no-resep" value="<?php echo $k->nomor_resep ?>"/>
                    <div class="form-group">
                        
                  <label>Pilih obat</label>
                    <select class="form-control" id="view_obat" name="pilih_pilih_obat">
                    <!-- generated otomatis -->
                  </select>
                </div>  
                        <input type="text" style="display:none;" name="kode_psn" value="<?php echo $k->kode_psn; ?>">
                <div class="form-group">
                    <label>Jumlah obat yang akan diberikan</label>
                    <input type="number" placeholder="Jumlah obat" id="qty-obat" name="jumlah_obat" class="form-control" style="width:30%;" value="1">
                </div>
                <!-- text input -->
                  
                <div class="form-group">
                  <label>Masukkan dosis</label>
                  <textarea class="form-control" id="dosis" name="dosis" rows="5" placeholder="Enter ..."></textarea>
                </div>
          <!-- /.box -->
                  <?php endforeach; ?>
                <div class="box-footer">
                <button type="button" id="btn-tambah" onclick="sendTotable()" class="btn btn-primary">Tambah</button>
              </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <table class="table table-bordered table-striped" id="table-obat">
                <thead>

                <tr>
                  <th>Nama Obat</th>
                  <th>Qty</th>
                  <th>Dosis</th>
                  <th>Action</th>
                </tr>

                </thead>
                <tbody>
                  

              </tbody>
              
              </table>
              <button type="button" id ="submit-obat" onclick="submitObat()" class="btn btn-primary pull-right" disabled>
              Submit
                  </button><br><br><br>
                    </div>
                  </div>

                
        </form>        
             </div>
             </div>
        </div>
</section><!-- /.content -->
<?php
$this->load->view('page/template/js');
?>
<script src="<?php echo base_url('assets/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
    
    
      
      function sendTotable(){
      var nama = $("#view_obat option:selected").text();
      var qty = $('#qty-obat').val();
      var dosis = $("#dosis").val();
      var rowCount = $('#table-obat tr').length;

      if(nama == "" || qty == "" || dosis==""){
        alert("Error : Pastikan anda sudah memasukan data dengan benar");
      }else{
        $('#table-obat tr:last').after('<tr id="row_'+rowCount+'"><td class="nama-obat">'+nama+'</td><td class="qty">'+qty+'</td><td class="dosis">'+dosis+'</td><td><button type="button" class="btn btn-danger" onclick="removeRow('+rowCount+')"><i class="fa fa-trash"></i></button></td></tr>');
        isEnable();
      }
    }
    function removeRow(row){
      $("#row_"+row).remove();
      isEnable();
    }
    function submitObat(){

      var noPdf = $('#no-pdf').val();
      var noResep = $('#no-resep').val();

      var myObj = new Object();
      myObj.noPdf = noPdf;
      myObj.noResep = noResep;

      var detail = [];
      $('#table-obat tr').each(function() {
        var nama = $(this).find(".nama-obat").html(); 
        var qty = $(this).find(".qty").html(); 
        var dosis = $(this).find(".dosis").html();
         
        if(typeof(nama) != "undefined" ){

          var d = new Object();
          d.nama = nama;
          d.dosis = dosis;
          d.qty = parseInt(qty);

          detail.push(d);
        }  
        
      });

      var obj = new Object();
      obj.data = myObj;
      obj.detail = detail;

      var myString = JSON.stringify(obj);
      console.log(myString);
      var pathname = window.location.pathname.split('/');
      $.ajax({
        type : "POST",
        dataType: "JSON",
        url : "<?php echo site_url('apoteker/proses_obat_new'); ?>",
        data : obj,
        success:function(data){

          var base_url = window.location.origin;
          window.location.replace(base_url+"/"+pathname[1]+"/pembayaran/bayar/"+data.no_pdf+"/"+data.kode_psn);
              
         
            }
          
        })
    }
    function isEnable(){
      var rowCount = $('#table-obat tr').length;
      console.log("rowCount : "+rowCount);
      if(rowCount >= 2){
        $("#submit-obat").prop('disabled', false);
      }else{
        $("#submit-obat").prop('disabled', true);
      }
    }
</script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/js/raphael-min.js') ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.min.js') ?>" type="text/javascript"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/sparkline/jquery.sparkline.min.js') ?>" type="text/javascript"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>" type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/knob/jquery.knob.js') ?>" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker.js') ?>" type="text/javascript"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/bootstrap-datepicker.js') ?>" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>" type="text/javascript"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/pages/dashboard.js') ?>" type="text/javascript"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/demo.js') ?>" type="text/javascript"></script>

<?php
$this->load->view('page/template/foot');
?>
