<?php
$this->load->view('page/template/head');
?>

<!--tambahkan custom css disini-->
<!-- iCheck -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/flat/blue.css') ?>" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.css') ?>" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.css') ?>" rel="stylesheet" type="text/css" />
<!-- Date Picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>" rel="stylesheet" type="text/css" />

<?php
$this->load->view('page/template/topbar');
$this->load->view('page/template/sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <!-- left column -->
      <h1>Rekam Medis</h1>
      
      <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Data pasien</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-user margin-r-5"></i> Nama Pasien</strong>

              <p class="text-muted">
              <?php echo $data->nama_psn ?>
              </p>

              <hr>
              <strong><i class="fa fa-mars margin-r-5"></i> Jenis Kelamin</strong>

              <p class="text-muted">
              <?php echo $data->gender_psn ?>
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>

              <p class="text-muted"><?php echo $data->alamat_psn ?></p>

              
            </div>
            <!-- /.box-body -->
          </div>
          <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Riwayat pasien</h3>
          </div>
          <div class="box-body">
            <table id="tabel-riwayat" class="table table-bordered table-striped table-responsive">
              <thead>
              
                <tr>
                  <th>Tanggal Resep</th>
                  <th>Diagnosa</th>
                  <th>Keterangan</th>
                </tr>
                
              </thead>
              <tbody>
                <?php foreach($resep as $r): ?>
                  <tr>
                    <td><?php echo $r->tanggal_resep; ?></td>
                    <td><?php echo $r->diagnosa; ?></td>
                    <td><?php echo $r->detail_resep; ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Anamnesa</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label>Pemeriksaan Fisik</label>
                <textarea class="form-control" id = "p_fisik" rows="5" placeholder="Pemeriksaan fisik..." disabled><?php echo $data->pemeriksaan_fisik ?></textarea>
            </div>
            <div class="form-group">
              <label>Pemeriksaan Penunjang</label>
                <textarea class="form-control" id = "p_penunjang" rows="5" placeholder="Pemeriksaan penunjang..." disabled><?php echo $data->pemeriksaan_penunjang ?></textarea>
            </div>
          </div>
        </div>
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Diagnosa</h3>
          </div>
          <div class="box-body">
          <div class="form-group">
                <label>Terapi (Obat)</label>
                <input type="text" value="<?php echo $data->diagnosa?>" class="form-control" disabled/>
              </div>
            <div class="form-group">
              <label>Edukasi</label>
                <textarea class="form-control" id="edukasi" rows="5" placeholder="Edukasi..." disabled><?php echo $data->detail_resep ?></textarea>
            </div>
            
          </div>
        </div>
      </div>
    </div>
</section><!-- /.content -->
</section>
<!-- Main content -->
<?php
$this->load->view('page/template/js');
?>

<script src="<?php echo base_url('assets/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/js/raphael-min.js') ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.min.js') ?>" type="text/javascript"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/sparkline/jquery.sparkline.min.js') ?>" type="text/javascript"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>" type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/knob/jquery.knob.js') ?>" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker.js') ?>" type="text/javascript"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/bootstrap-datepicker.js') ?>" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>" type="text/javascript"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/pages/dashboard.js') ?>" type="text/javascript"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/demo.js') ?>" type="text/javascript"></script>

<?php
$this->load->view('page/template/foot');
?>
